/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your application specific code will go here
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojmodule-element-utils', 'ojs/ojmodule-element', 'ojs/ojrouter', 'ojs/ojknockout', 'ojs/ojarraytabledatasource'],
  function(oj, ko, moduleUtils) {
     function ControllerViewModel() {
       var self = this;

      // Media queries for repsonsive layouts
      var smQuery = oj.ResponsiveUtils.getFrameworkQuery(oj.ResponsiveUtils.FRAMEWORK_QUERY_KEY.SM_ONLY);
      self.smScreen = oj.ResponsiveKnockoutUtils.createMediaQueryObservable(smQuery);

       // Router setup
       self.router = oj.Router.rootInstance;
       self.router.configure({
         'login': {label: 'Login', isDefault: true},
         'incidents': {label: 'Incidents'}
       });
      oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();

      self.moduleConfig = ko.observable({'view':[], 'viewModel':null});

      self.loadModule = function() {
        ko.computed(function() {
          var name = self.router.moduleConfig.name();
          var viewPath = 'views/' + name + '.html';
          var modelPath = 'viewModels/' + name;
          var masterPromise = Promise.all([
            moduleUtils.createView({'viewPath':viewPath}),
            moduleUtils.createViewModel({'viewModelPath':modelPath})
          ]);
          masterPromise.then(
            function(values){
              self.moduleConfig({'view':values[0],'viewModel':values[1]});
            }
          );
        });
      };

      // Navigation setup
      if (window.sessionStorage.getItem("logged") =='true') {
        var navData = [
          {name: 'Incidents', id: 'incidents',
          iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-fire-icon-24'}
        ];

      } else {
        var navData = [
          {name: 'Login', id: 'login',
          iconClass: 'oj-navigationlist-item-icon demo-icon-font-24 demo-chart-icon-24'}
        ];
      }
      self.navDataSource = new oj.ArrayTableDataSource(navData, {idAttribute: 'id'});

      // Header
      // Application Name used in Branding Area
      self.appName = ko.observable("Demo Web");
      // User Info used in Global Navigation area

      if (window.sessionStorage.logged == 'true') {
        var userSession = window.sessionStorage.getItem("user_session");
        var userDetail = JSON.parse(userSession);
        console.log(userDetail);
        self.userLogin = ko.observable(userDetail.first_name);
      } else {
        self.userLogin = ko.observable("Login");

        oj.Router.rootInstance.go('login');
      }

      self.signOut = function (){
        sessionStorage.removeItem("logged");
        oj.Router.rootInstance.go('login');
        location.reload();
      };

      if (window.sessionStorage.getItem("logged") =='true') {
        $("#userDetailNav").css("display","block");
      }

      var setlang = window.sessionStorage.getItem("web_lang");
      oj.Config.setLocale(setlang,
            function() {
              console.log(setlang);
              $('html').attr('lang', setlang);
            }
      );
     }

     return new ControllerViewModel();
  }
);
