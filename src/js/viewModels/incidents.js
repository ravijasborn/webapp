/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojchart', 'ojs/ojtoolbar', 'ojs/ojarraydataprovider'],
 function(oj, ko, $) {

    function IncidentsViewModel() {
      var self = this;
      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };

      if (window.sessionStorage.logged == 'true') {
        var userSession = window.sessionStorage.getItem("user_session");
        var userDetail = JSON.parse(userSession);
      } else {
        oj.Router.rootInstance.go('login');
      }

      /* chart */
        self.stackValue = ko.observable('off');
        self.orientationValue = ko.observable('vertical');

        /* chart data */
        var barSeries = [
                          {name: "Highest data of 2018", items: [42,43,54,32,43,65,23,65,54,76,78,89]},
                          {name: "Lowest data of 2018", items: [21,31,44,11,12,32,13,55,50,72,68,79]}
                        ];

        var barGroups = ["Jan","Feb","March","April","May","June","July","Aug","Sept","Oct","Nov","Dec"];

        self.barSeriesValue = ko.observableArray(barSeries);
        self.barGroupsValue = ko.observableArray(barGroups);

        /*  drill down */
        this.gotoList = function(event, ui) {
            document.getElementById("chart-container").currentItem = null;
            self.slide();
        };

        this.content = ko.observable("");

        this.gotoContent = function(event) {
            if (event.detail.value != null && event.detail.value.length > 0)
            {
              console.log(event.detail.selectionData[0]);
                var row = event.detail.value;
                var values = event.detail.selectionData[0]
                var content = row+', '+values.data+'%';
                self.content(content);
                self.slide();
            }
        };
        self.slide = function() {
            $("#chart-container").toggleClass("demo-page1-hide");
            $("#page2").toggleClass("demo-page2-hide");
        }
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new IncidentsViewModel();
  }
);
