/**
 * @license
 * Copyright (c) 2014, 2018, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'appController', 'ojs/ojmodule-element-utils', 'ojs/ojdatagrid', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojbutton','ojs/ojrouter','ojs/ojknockout','ojs/ojcheckboxset','ojs/ojvalidationgroup','ojs/ojanimation','ojtranslations/nls/ojtranslations'],
 function(oj, ko, $) {

    function LoginViewModel() {
      var self = this;
      // Below are a set of the ViewModel methods invoked by the oj-module component.
      // Please reference the oj-module jsDoc for additional information.

      /**
       * Optional ViewModel method invoked after the View is inserted into the
       * document DOM.  The application can put logic that requires the DOM being
       * attached here.
       * This method might be called multiple times - after the View is created
       * and inserted into the DOM and after the View is reconnected
       * after being disconnected.
       */
      self.connected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after the View is disconnected from the DOM.
       */
      self.disconnected = function() {
        // Implement if needed
      };

      /**
       * Optional ViewModel method invoked after transition to the new View is complete.
       * That includes any possible animation between the old and the new View.
       */
      self.transitionCompleted = function() {
        // Implement if needed
      };
      self.groupValid = ko.observable();
      self.tracker = ko.observable();
      self.firstName = ko.observable();
      self.passWord = ko.observable();
      self.submit = "Login";
      self.username = "Username";

      if (window.sessionStorage.logged =='true') {
        oj.Router.rootInstance.go('incidents');
      }

      self.signIn = function() {
        var tracker = document.getElementById("tracker");
        if (tracker.valid === "valid") {
          fetch("localStorage/users.json", { //fetch data from localStorage.
             method: 'get',
             headers: {
               "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
             }
           },
           {
              mode: 'cors',
              headers: {
                'Access-Control-Allow-Origin':'*'
              }
            })
           .then(function (response) {
             response.json().then(function(res) {
               var responseData = JSON.stringify(res);
               res.map(function(arr){
                 if (arr.username==self.firstName() && arr.password==self.passWord()) {
                   console.log('Success');
                   sessionStorage.setItem("user_session", JSON.stringify(arr));
                   window.sessionStorage.logged = 'true';
                   oj.Router.rootInstance.go('incidents');
                   location.reload();
                 } else {
                   alert('Invalid login credential!')
                   console.log('failed');
                 }
               })
             });
           })
           .catch(function (error) {
             console.log('Request failed', error);
           });
        }
        else {
           // show messages on all the components
           tracker.showMessages();
           tracker.focusOn("@firstInvalidShown");
        }
      };

      /* set locale */
      // This function loads the menu items.
      self.localeLabel = ko.observable();
      self.menuNames = ko.observableArray([]);

      self.changeLabel = function (evt) {
        self.localeLabel(oj.Translations.getTranslatedString(evt.target.value));
      }
      self.setLang = function(evt) {
        var newLang = '';
        var lang = evt.currentTarget.id;
        console.log(lang)
        switch (lang){
          case 'français':
            newLang = 'fr-FR';
            break;
          default:
            newLang = 'en-US';
        }
        oj.Config.setLocale(newLang,
              function() {
                console.log('chhoese');
                $('html').attr('lang', newLang);
                sessionStorage.removeItem("web_lang");
                sessionStorage.setItem("web_lang", newLang);
                location.reload();
              }
        );
      }

      var setlang = window.sessionStorage.getItem("web_lang")
      if (setlang=='en-US') {
        var toolbarData = {
          // user name in toolbar
          // "userName": "ravi",
          "toolbar_buttons": [
            {
              "label": "français",
              "iconClass": "demo-fr-flag-icon",
              "url": "fr/index.html"
            }
          ]
        }
      } else if (setlang=='fr-FR') {
        var toolbarData = {
          "toolbar_buttons": [
            {
              "label": "english",
              "iconClass": "demo-en-flag-icon",
              "url": "#"
            }
          ]
        }
      } else {
        var toolbarData = {
          "toolbar_buttons": [
            {
              "label": "français",
              "iconClass": "demo-fr-flag-icon",
              "url": "fr/index.html"
            }
          ]
        }
      }
      self.toolbarButtons = toolbarData.toolbar_buttons;
    }

    /*
     * Returns a constructor for the ViewModel so that the ViewModel is constructed
     * each time the view is displayed.  Return an instance of the ViewModel if
     * only one instance of the ViewModel is needed.
     */
    return new LoginViewModel();
  }
);
