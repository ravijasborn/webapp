define({
  "root": {
    "oj-ojinputtext": {
      Username: "{0}",
    },
    "oj-span": {
      Username: "Valeur",
      tooltipRequired: "Valeur"
    },
    "oj-ojChart": {
      labelDefaultGroupName: "Groupe {0}",
      labelSeries: "yuffutc",
      labelGroup: "Groupe",
      labelDate: "Date",
      labelValue: "Valeur",
      labelTargetValue: "Cible",
      labelX: "X",
      labelY: "Y",
      labelZ: "Z",
      labelPercentage: "Pourcentage",
      labelLow: "Faible",
      labelHigh: "Elevé",
      labelOpen: "Ouvrir",
      labelClose: "Fermer",
      labelVolume: "Volume",
      labelQ1: "Q1",
      labelQ2: "Q2",
      labelQ3: "Q3",
      labelMin: "Min.",
      labelMax: "Max.",
      labelOther: "Autre",
      tooltipPan: "Panoramique",
      tooltipSelect: "Sélection par rectangle de sélection",
      tooltipZoom: "Zoom par rectangle de sélection",
      componentName: "Graphique",
      highestDataOf2018:"Zoom par rectangle de sélection"
    },
},
"label": "Canadian French message here",
  "fr-FR": true
});
